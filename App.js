/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState} from 'react';
import type { Node } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import { HeaderComponent, DataComponent } from './components/components';

const App: () => Node = () => {
  const [content, setContent] = useState('Content initialized value!');
  const [data, setData] = useState('');

  useEffect(() => {
    setData(`Content was ${content.length} chars long`);
  }, []);


  return (
    <SafeAreaView style={{backgroundColor: '#FDE987'}}>
      <StatusBar />
      <ScrollView contentInsetAdjustmentBehavior="automatic">
      <Text style={{fontSize: 30}}>{data}</Text>
        <HeaderComponent title="My app" description={content} />
        <DataComponent callback={setContent} />
        <View />
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
});

export default App;
