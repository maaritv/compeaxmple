/* eslint-disable prettier/prettier */
import React, {useEffect, useState} from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';

export const HeaderComponent = ({title, description}) => {
  return (
    <View style={{backgroundColor: '#ABCD67'}}>
      <View>
        <Text>{title}</Text>
      </View>
      <View>
        <Text>{description}</Text>
      </View>
    </View>
  );
};

export const DataComponent = ({callback}) => {
  const [description, setDescription] = useState('');

  useEffect(() => {
    let mounted = true;
    callback(description);
    return () => (mounted = false);
  }, [description, callback]);

  return (
    <View style={{backgroundColor: '#987687'}}>
      <Text>Description is now! {description}</Text>
      <TextInput
        style={styles.input}
        onChangeText={setDescription}
        value={description}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    height: 70,
    width: 400,
    fontSize: 14,
    borderWidth: 1,
  },
});
